#!/bin/bash
set -e

#
# Sets Logstash shipper.
#

apt-get install -y curl

# logstash
curl -O https://download.elasticsearch.org/logstash/logstash/logstash-1.4.2.tar.gz
tar zxvf logstash-1.4.2.tar.gz -C /opt
ln -s /opt/logstash-1.4.2 /opt/logstash

# config
mkdir -p /etc/logstash
mkdir -p /var/log/logstash
mkdir -p /var/run/logstash/syncdb
cp conf/logstash.shipper.conf /etc/logstash/shipper.conf
cp init/logstash-shipper /etc/init.d/
chmod +x /etc/init.d/logstash-shipper

update-rc.d logstash-shipper defaults
service logstash-shipper start

echo "Logstash shipper service up and running"

